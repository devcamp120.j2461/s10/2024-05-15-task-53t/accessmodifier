import p1.A;
import p2.C;

public class D {
    public void test() {
        A a = new A();
        a.i;
        a.j;
        a.k;
        a.l;
    
        C c1 = new C(); // c will have 6 properties : m,n declared in C. i,j,k,l declared in parent.   
        c1.test();                         
    }
}
